﻿using AutoMapper;
using Currencies.Business.Services.Currencies;
using Currencies.Web.Models.Currencies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Currencies.Web.Controllers
{
    [ApiController]
    [Route("api/currencies")]
    public class CurrenciesController : ControllerBase
    {
        private readonly ICurrenciesService currenciesService;
        private readonly IMapper mapper;
        public CurrenciesController(ICurrenciesService _currenciesService, 
                                    IMapper _mapper)
        {
            currenciesService = _currenciesService ?? throw new ArgumentNullException(nameof(_currenciesService));
            mapper = _mapper ?? throw new ArgumentNullException(nameof(_mapper));
        }

        [HttpGet]
        public async Task<IEnumerable<CurrenciesResponse>> GetCurrencies(CancellationToken cancellationToken)
        {
            var currencies = await currenciesService.GetCurrencies(cancellationToken);
            var currenciesResponse = mapper.Map<IEnumerable<CurrenciesResponse>>(currencies);
            return currenciesResponse;
        }

        [HttpGet("{currency}")]
        public async Task<ActionResult<CurrenciesResponse>> GetCurrencyById([FromRoute] int id, CancellationToken cancellationToken)
        {
            var currency = await currenciesService.GetCurrencyById(id, cancellationToken);

            if (currency == null)
            {
                return NotFound();
            }

            var currencyResponse = mapper.Map<CurrenciesResponse>(currency);
            return currencyResponse;
        }
        [HttpPost]
        public async Task InsertCurrencies(CancellationToken cancellationToken)
        {
            await currenciesService.InsertCurrencies(cancellationToken);
        }
    }
}
