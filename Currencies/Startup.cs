using AutoMapper;
using Currencies.Business.Services.Currencies;
using Currencies.Business.Services.Currencies.XmlService;
using Currencies.DataAccess;
using Currencies.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currencies.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Business.Mapper.Currencies.CurrencyProfile());
                mc.AddProfile(new Currencies.Web.Mapper.Currencies.CurrencyProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddDbContext<CurrenciesContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("CurrenciesConnection")));
            services.AddMvc();
            services.AddTransient<ICurrenciesService, CurrenciesService>();
            services.AddTransient<ICurrenciesRepository, CurrenciesRepository>();
            services.AddTransient<IXmlService, XmlService>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
