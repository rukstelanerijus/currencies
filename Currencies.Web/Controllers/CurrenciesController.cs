﻿using AutoMapper;
using Currencies.Business.Services;
using Currencies.Web.Models.Currencies;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Currencies.Web.Controllers
{
    public class CurrenciesController : Controller
    {
        private readonly ICurrenciesService _currenciesService;
        private readonly IMapper _mapper;

        public CurrenciesController(ICurrenciesService currenciesService,
                                    IMapper mapper)
        {
            _currenciesService = currenciesService;
            _mapper = mapper;
        }
        
        public async Task<IActionResult> Currencies()
        {
            var currencies = await _currenciesService.GetCurrencies();
            var currencyViewModel = _mapper.Map<IEnumerable<CurrencyViewModel>>(currencies);
            
            return View(currencyViewModel);
        }
    }
}
