﻿using AutoMapper;
using Currencies.Business.Services;
using Currencies.Web.Models.Currencies;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currencies.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly ICurrenciesService _currenciesService;
        private readonly IMapper _mapper;

        public HomeController(ICurrenciesService currenciesService,
                                    IMapper mapper)
        {
            _currenciesService = currenciesService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Converter()
        {
            var currenciesResponse = await _currenciesService.GetLastCurrencies();
            var currencyViewModel = _mapper.Map<IEnumerable<CurrencyViewModel>>(currenciesResponse);

            return View(currencyViewModel);
        }

        public async Task<IActionResult> InsertCurrencies()
        {
            await _currenciesService.InsertCurrencies();
            return View("ImportSuccess");
        }

        public IActionResult Convert(decimal fromAmount, decimal fromRate, decimal toRate)
        {
            var model = new ConversionResultViewModel();
            model.Result = _currenciesService.CurrenciesConverter(fromAmount, fromRate, toRate);
                       
            return View("Result", model);
        }
    }
}
