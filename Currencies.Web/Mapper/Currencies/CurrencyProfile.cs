﻿using AutoMapper;
using Currencies.Web.Models.Currencies;

namespace Currencies.Web.Mapper.Currencies
{
    public class CurrencyProfile: Profile
    {
        public CurrencyProfile()
        {
            CreateMap<Business.Models.Currency, CurrencyViewModel>();
        }
    }
}
