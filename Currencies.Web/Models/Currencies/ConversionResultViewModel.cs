﻿namespace Currencies.Web.Models.Currencies
{
    public class ConversionResultViewModel
    {
        public decimal Result { get; set; }
    }
}
