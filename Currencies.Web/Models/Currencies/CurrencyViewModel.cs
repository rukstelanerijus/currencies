﻿using System;

namespace Currencies.Web.Models.Currencies
{
    public class CurrencyViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public decimal Rate { get; set; }
        public DateTime Date { get; set; }
    }
}
