﻿using Currencies.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Currencies.DataAccess.Repositories
{
    public interface ICurrenciesRepository
    {
        Task<IEnumerable<Currency>> GetCurrencies();
        Task<IEnumerable<Currency>> GetLastCurrencies();
        Task InsertCurrencies(IEnumerable<Currency> currencies);
    }
}
