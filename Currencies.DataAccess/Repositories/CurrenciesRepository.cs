﻿using Currencies.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currencies.DataAccess.Repositories
{
    public class CurrenciesRepository : ICurrenciesRepository
    {
        private readonly CurrenciesContext _currenciesContext;
        
        public CurrenciesRepository(CurrenciesContext currenciesContext)
        {
            _currenciesContext = currenciesContext;
        }

        public async Task<IEnumerable<Currency>> GetCurrencies()
        {
            return await _currenciesContext.Currencies.OrderByDescending(x=>x.Date).ToListAsync();
        }

        public async Task<IEnumerable<Currency>> GetLastCurrencies()
        {
            var date = DateTime.UtcNow.Date;
            var currencies = await _currenciesContext.Currencies.Where(x => x.Date == date).ToListAsync();

            if (currencies.Count == 0)
            {
                date = date.AddDays(-1);
                currencies = await _currenciesContext.Currencies.Where(x => x.Date == date).ToListAsync();
            }

            return currencies;
        }

        public async Task InsertCurrencies(IEnumerable<Currency> currency)
        {
            if (currency == null)
            {
                throw new ArgumentNullException(nameof(currency));
            }

            await _currenciesContext.Currencies.AddRangeAsync(currency);
            await _currenciesContext.SaveChangesAsync();
        }
    }
}
