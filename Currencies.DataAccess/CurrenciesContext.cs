﻿using Currencies.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Currencies.DataAccess
{
    public class CurrenciesContext : DbContext
    {
        public CurrenciesContext(DbContextOptions<CurrenciesContext> options): base(options) {}

        public DbSet<Currency> Currencies { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Currency>().ToTable("Currencies");
        }
    }
}
