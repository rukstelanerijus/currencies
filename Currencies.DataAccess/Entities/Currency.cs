﻿using System;

namespace Currencies.DataAccess.Entities
{
    public class Currency
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public decimal Rate { get; set; }
        public DateTime Date { get; set; }
    }
}
