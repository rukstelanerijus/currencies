﻿using AutoMapper;

namespace Currencies.Business.Mapper.Currencies
{
    public class CurrencyProfile : Profile
    {
        public CurrencyProfile()
        {
            CreateMap<Business.Models.Currency, DataAccess.Entities.Currency>();
            CreateMap<DataAccess.Entities.Currency, Business.Models.Currency>();
        }
    }
}
