﻿using System;

namespace Currencies.Business.Models
{
    public class Currency
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public decimal Rate { get; set; }
        public DateTime Date { get; set; }
    }
}
