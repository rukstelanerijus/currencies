﻿using AutoMapper;
using Currencies.Business.Models;
using Currencies.Business.Services.Currencies.XmlService;
using Currencies.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currencies.Business.Services
{
    public class CurrenciesService : ICurrenciesService
    {
        private readonly ICurrenciesRepository _currencyRepository;
        private readonly IMapper _mapper;
        private readonly IXmlService _xmlService;

        public CurrenciesService(ICurrenciesRepository currenciesRepository, 
                                 IMapper mapper,
                                 IXmlService xmlService)
        {
            _currencyRepository = currenciesRepository;
            _mapper = mapper;
            _xmlService = xmlService;
        }

        public async Task<IEnumerable<Currency>> GetCurrencies()
        {
            var currencies = await _currencyRepository.GetCurrencies();
            return _mapper.Map<IEnumerable<Currency>>(currencies);
        }

        public async Task InsertCurrencies()
        {
            var existingCurrencies = await _currencyRepository.GetLastCurrencies();

            var currentDate = DateTime.UtcNow.Date;

            if (existingCurrencies?.Count() > 0 && existingCurrencies?.FirstOrDefault().Date == currentDate)
            {
                return;
            }

            var currencies = _xmlService.RetrieveCurrenciesFromXml(existingCurrencies);
            var currenciesDao = _mapper.Map<IEnumerable<DataAccess.Entities.Currency>>(currencies);
            await _currencyRepository.InsertCurrencies(currenciesDao);
        }
     
        public async Task<IEnumerable<Currency>> GetLastCurrencies()
        {
            var currencies = await _currencyRepository.GetLastCurrencies();
            return _mapper.Map<IEnumerable<Currency>>(currencies);
        }

        public decimal CurrenciesConverter(decimal fromAmount, decimal fromRate, decimal toRate)
        {
            return Math.Round(((toRate * fromAmount) / fromRate), 2);
        }
    }
}
