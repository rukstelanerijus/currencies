﻿using Currencies.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Currencies.Business.Services.Currencies.XmlService
{
    public interface IXmlService
    {
        IEnumerable<Currency> RetrieveCurrenciesFromXml(IEnumerable<DataAccess.Entities.Currency> existingCurrencies);
    }
}
