﻿using Currencies.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Currencies.Business.Services
{
    public interface ICurrenciesService
    {
        Task<IEnumerable<Currency>> GetCurrencies();
        Task InsertCurrencies();
        Task<IEnumerable<Currency>> GetLastCurrencies();
        decimal CurrenciesConverter(decimal fromAmount, decimal fromRate, decimal toRate);
    }
}
