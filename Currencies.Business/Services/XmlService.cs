﻿using Currencies.Business.Models;
using Currencies.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Currencies.Business.Services.Currencies.XmlService
{
    public class XmlService : IXmlService
    {
        public XmlService()
        {
        }

        public IEnumerable<Currency> RetrieveCurrenciesFromXml(IEnumerable<DataAccess.Entities.Currency> existingCurrencies)
        {
            XmlReader xmlReader = XmlReader.Create("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");
            DateTime date = DateTime.UtcNow.Date;
            var currencies = new List<Currency>();

            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "Cube"))
                {
                    if (xmlReader.HasAttributes)
                    {
                        if (xmlReader.GetAttribute("time") != null)
                        {
                            date = DateTime.Parse(xmlReader.GetAttribute("time"));

                            if (existingCurrencies?.Count() > 0 && existingCurrencies?.FirstOrDefault().Date == date)
                            {
                                break;
                            }
                            
                            continue;
                        }
                        var currency = new Currency
                        {
                            Value = xmlReader.GetAttribute("currency"),
                            Rate = decimal.Parse((xmlReader.GetAttribute("rate")).Replace('.', ',')),
                            Date = date
                    };
                        currencies.Add(currency);
                    }
                }
            }
            return currencies;
        }
    }
}
